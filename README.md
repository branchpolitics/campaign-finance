# Open Campaign Finance

This is an open-source project with a mission to organize and summarize campaign finance data for candidates seeking office in Georgia. This repository is managed by Branch Politics \(www.branch.vote\) and members of the Atlanta community.
