import datetime
import json
from typing import final
import requests
import shutil
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import boto3
# https://pdfminersix.readthedocs.io/en/latest/index.html
import pdfminer
from pdfminer.layout import LAParams
from pdfminer.high_level import extract_pages


def init_output(incoming_doc):
    doc = {}
    doc['input'] = incoming_doc
    doc['documentid'] = incoming_doc['documentid']
    doc['filerid'] = incoming_doc['filerid']
    doc['status'] = 'predownload'
    return doc


def get_pdf(final_doc):
    url = final_doc['input']['documentUrl']
    documentid = final_doc['documentid']
    r = requests.get(url, verify=False, stream=True)
    r.raw.decode_content = True
    file_name = documentid + '.pdf'
    with open(file_name, 'wb') as f:
        shutil.copyfileobj(r.raw, f)
    return file_name


def get_pages(file_path, pageno=-1):
    params = LAParams(line_overlap=0.5, char_margin=2.0, line_margin=0.5,
                      word_margin=0.1, boxes_flow=0.5, detect_vertical=False, all_texts=True)
    if pageno == -1:
        pages = extract_pages(file_path, laparams=params)
    else:
        pages = extract_pages(file_path, page_numbers=[
                              pageno], maxpages=1, laparams=params)
    return pages


def fuzzy_match_coord(x_target, y_target, x_pdf, y_pdf, tolerance=10):
    if (x_target-tolerance <= x_pdf <= x_target+tolerance) and \
            (y_target-tolerance <= y_pdf <= y_target+tolerance):
        return True
    else:
        return False


def get_elements(pages):
    doc = []
    i = 0
    for page_layout in pages:
        i = i + 1
        page_type_number = ''
        form_elements = []
        for element in page_layout:
            if isinstance(element, pdfminer.layout.LTTextBoxHorizontal):
                if fuzzy_match_coord(516, 778, element.x0, element.y0):
                    page_type_number = element.get_text().strip('\n ')
            if isinstance(element, pdfminer.layout.LTFigure):
                for sub in element:
                    if isinstance(sub, pdfminer.layout.LTTextBoxHorizontal):
                        parsed_form = (round(sub.x0), round(
                            sub.y0), sub.get_text().strip('\n '))
                        form_elements.append(parsed_form)
        doc.append((i, page_type_number, form_elements))
    return doc


def match_page(pages, coords):
    page_mapping = {}
    page_mapping['_unmapped'] = []
    for page in pages:
        try:
            page_mapping[page[0]] = coords["page_mapping"][page[1]]
        except KeyError:
            # TODO: raise error for pages not mapping
            pass
    return page_mapping


def match_page_elements(pdf_page, coords_page):
    page_dict = {}
    page_dict['_unmapped'] = []
    for element in pdf_page:
        match = False
        for k, v in coords_page['fields'].items():
            try:
                if 'tolerance' in v:
                    fuzzy_match = fuzzy_match_coord(
                        v['x'], v['y'], element[0], element[1], tolerance=v['tolerance'])
                else:
                    fuzzy_match = fuzzy_match_coord(
                        v['x'], v['y'], element[0], element[1])
                if fuzzy_match:
                    # TODO: adjustable fuzzy match tolerances
                    if k in page_dict:
                        # TODO: action for collision
                        print('COLLISION')
                    page_dict[k] = element[2]
                    match = True
            except:
                # TODO: catch error
                pass
        if not match:
            page_dict['_unmapped'].append(element)
    return page_dict


def match_doc_elements(pages, page_map, coords, page_list=[0]):
    ccdr_dict = {}
    if page_list == [0]:
        page_list = range(1, len(pages))
    for page in pages:
        page_number = page[0]
        if page_number in page_list:
            page_type = page_map[page_number]
            coords_page = coords[page_type]
            page_dict = match_page_elements(page[2], coords_page)
            ccdr_dict.setdefault(page_type, {})
            ccdr_dict[page_type][page_number] = page_dict
    return ccdr_dict


def strip_money(money):
    money = money.replace('$', '').replace(',', '')
    if money.startswith('(') and money.endswith(')'):
        money = '-' + money.replace('(', '').replace(')', '')
    return money


def process_contributions_page(page, is_first):
    blocks = ['a_', 'b_', 'c_', 'd_']
    page_contributions = {block: {} for block in blocks}
    for field in page:
        prefix = field[:2]
        if prefix in blocks:
            page_contributions[prefix][field[2:]] = page[field]
    page_contributions = list(page_contributions.values())
    if is_first:
        page_contributions = page_contributions[:3]
    calcuated_total_cash = 0
    calculated_total_value = 0
    for contribution in page_contributions:

        calcuated_total_cash = round(
            calcuated_total_cash + float(strip_money(contribution['cash'])), 2)
        calculated_total_value = round(
            calculated_total_value + float(strip_money(contribution['value'])), 2)
    # TODO: validate page totals
    page_total_cash = float(strip_money(page['page_total_cash']))
    page_total_value = float(strip_money(page['page_total_value']))
    if page_total_cash != calcuated_total_cash:
        # print('PAGE TOTAL CASH ERROR')
        # print('page', page['footer_page'])
        # print('page_total_cash', page_total_cash)
        # print('calcuated_total_cash', calcuated_total_cash)
        pass
    if page_total_value != calculated_total_value:
        # print('PAGE TOTAL VALUE ERROR')
        # print('page', page['footer_page'])
        # print('page_total_value', page_total_value)
        # print('calculated_total_value', calculated_total_value)
        pass
    return page_contributions


def process_all_contributions(ccdr):
    contributions = []
    # process first page
    contributions_first_page = ccdr['itemized_contributions'][4]
    new_contributions = process_contributions_page(
        contributions_first_page, is_first=True)
    contributions = contributions + new_contributions
    # process additional pages
    if 'itemized_contributions_2' in ccdr:
        contributions_cont_pages = ccdr['itemized_contributions_2']
        for page in contributions_cont_pages:
            new_contributions = process_contributions_page(
                contributions_cont_pages[page], is_first=False)
            contributions = contributions + new_contributions
    return contributions


def collapse_checkboxes(ccdr_raw, sections, pdf_mapping):
    # TODO: collpase checkbox options
    for section in sections:
        checkbox_map = pdf_mapping[section]['checkbox_map']
        if section in ccdr_raw:
            for page in ccdr_raw[section]:
                for field in list(ccdr_raw[section][page]):
                    # using list(data) due to changing data keys in runtime
                    if field in checkbox_map:
                        new_field = checkbox_map[field]['map']
                        new_value = checkbox_map[field]['value']
                        if new_value == 'self_value':
                            new_value = ccdr_raw[section][page][field]
                        ccdr_raw[section][page][new_field] = new_value
                        del(ccdr_raw[section][page][field])
    return ccdr_raw


def clean_ccdr_fields(ccdr, pdf_mapping):
    data = {}
    data['errors'] = []
    data['raw_output'] = ccdr
    # collapse checks
    sections = ['title', 'summary_report',
                'itemized_contributions', 'itemized_contributions_2']
    ccdr = collapse_checkboxes(ccdr, sections, pdf_mapping)
    data['title'] = ccdr['title'][1]
    data['summary_report'] = ccdr['summary_report'][2]
    data['itemized_contributions'] = process_all_contributions(ccdr)
    return data


def validate_unmatched_fields(data):
    # Check for unmapped form fields
    for section in data['raw_output']:
        for page in data['raw_output'][section]:
            unmapped = data['raw_output'][section][page]['_unmapped']
            relevant_sections = [
                'title', 'summary_report', 'itemized_contributions', 'itemized_contributions_2']
            if unmapped and (section in relevant_sections):
                data['errors'].append((
                    'unmapped form fields', page, section, unmapped))
    return data


def validate_contribution_totals(data):
    calcuated_total_cash = 0
    calculated_total_value = 0
    for contribution in data['itemized_contributions']:
        calcuated_total_cash = round(
            calcuated_total_cash + float(strip_money(contribution['cash'])), 2)
        calculated_total_value = round(
            calculated_total_value + float(strip_money(contribution['value'])), 2)
    page_total_cash = float(strip_money(
        data['summary_report']['contributions_total_itemized_cash']))
    page_total_value = float(strip_money(
        data['summary_report']['contributions_total_itemized_inkind']))
    if page_total_cash != calcuated_total_cash:
        data['errors'].append(
            'total cash mismatch -  expected: ' + page_total_cash + 'calculated: ' + calcuated_total_cash)
    if page_total_value != calculated_total_value:
        data['errors'].append(
            'total est. value mismatch -  expected: ' + page_total_value + 'calculated: ' + calculated_total_value)

    return data


def ccdr_metadata(ccdr):
    metadata = {}
    # metadata['file_name'] = file_name
    metadata['file_date'] = ccdr['title']['date']
    metadata['file_type'] = ccdr['title']['report_type']
    metadata['reporting_deadline'] = ccdr['title']['reporting_period'] + \
        '_' + ccdr['title']['reporting_period_year']
    metadata['office'] = ccdr['title']['office']
    metadata['candidate'] = ccdr['title']['candidate']
    if 'committee' in ccdr:
        metadata['committee'] = ccdr['title']['committee']
    metadata['valid'] = False
    if len(ccdr['errors']) == 0:
        metadata['valid'] = True
    metadata['process_date'] = str(datetime.datetime.now())
    return metadata


def process_pdf(file_name, final_out):
    try:
        # parse pdf into text coordinates
        pages = get_pages(file_name)
        doc = get_elements(pages)
        # TODO: check if pdf pages are parseable
        if len(doc[0][2]) == 0:
            final_out['status'] = 'INCOMPLETE-pdf_coordinates_not_available'
            return final_out
        final_out['status'] = 'INCOMPLETE-pages_parsed'

        # load pdf coordinates and map pages
        with open('pdf_coords.json') as json_file:
            coords = json.load(json_file)
        page_map = match_page(doc, coords)
        final_out['status'] = 'INCOMPLETE-pages_mapped'

        # match coordinates to fields
        ccdr = match_doc_elements(doc, page_map, coords, page_list=[0])
        final_out['status'] = 'INCOMPLETE-elements_mapped'

        # clean form fields into organized data
        data = clean_ccdr_fields(ccdr, coords)
        data = validate_unmatched_fields(data)
        data = validate_contribution_totals(data)
        data['metadata'] = ccdr_metadata(data)
        if len(data['errors']) > 0:
            final_out['status'] = 'COMPLETE_WITH_ERRORS'
        else:
            final_out['status'] = 'COMPLETE'
        final_out['pdf_data'] = data
    except:
        return final_out
    finally:
        return final_out


def process_pipeline(input_doc=None, input_file_path=None, debug=False):
    try:
        if input_doc:
            # process using input_doc
            # initiate response json
            final_out = init_output(input_doc)
            # get pdf file as final_out['documentid']+'.pdf
            pdf_file_name = get_pdf(final_out)
        elif input_file_path:
            # process using file_path
            # TODO: init final_output when not passed doc
            pdf_file_name = input_file_path
        # get pdf file as final_out['documentid']+'.pdf
        final_out['status'] = 'INCOMPLETE-pdf_info_received'
        pdf_file_name = get_pdf(final_out)
        final_out['status'] = 'INCOMPLETE-pdf_downloaded'
        # process pdf
        final_out = process_pdf(pdf_file_name, final_out)
        if (not debug) and ('pdf_data' in final_out):
            if ('raw_output' in final_out['pdf_data']):
                del final_out['pdf_data']['raw_output']
        return final_out
    except:
        return final_out
    finally:
        return final_out

def api_input(input_doc, output='dynamodb_local'):
    pdf_info = input_doc['documentMetadata']
    final_out = process_pipeline(input_doc=pdf_info)
    final_out['city-upload-date'] = input_doc['city-upload-date']
    # TODO: switch to AWS dynamodb
    dynamodb = boto3.resource(
        'dynamodb', endpoint_url="http://localhost:8000", region_name="us-east-1")
    table = dynamodb.Table('campaign_finance_pdf_outputs')
    # overwrite doc if it's already been processed - easy handling reprocessing
    response = table.put_item(Item=final_out)



if __name__ == '__main__':
    # TODO: accept lambda trigger for doc
    sample_doc = {
        "documentid": "6A7151C0-A8B3-4386-BCB2-3EBA3DB89374",
        "documentname": "robb",
        "datesubmitted": "07/05/21",
        "documenttype": "Campaign Contribution Disclosure Report",
        "electionname": 'null',
        "filerid": "90ffe9d10c354c54aedba63e2abd1e84",
        "firstname": "Robert",
        "lastname": "Pitts",
        "committeename": 'null',
        "displayname": "Pitts, Robert",
        "officename": "Atlanta City Mayor",
        "documentUrl": "https://easycfangularapi.azurewebsites.net/documents/6A7151C0-A8B3-4386-BCB2-3EBA3DB89374/viewfinalredactedpdf"
    }
    # this can also be called with input_file_path='doc.pdf' for local testing
    final_out = process_pipeline(input_doc=sample_doc)

    # TODO: switch to AWS dynamodb
    dynamodb = boto3.resource(
        'dynamodb', endpoint_url="http://localhost:8000", region_name="us-east-1")
    table = dynamodb.Table('campaign_finance_pdf_outputs')
    # overwrite doc if it's already been processed - easy handling reprocessing
    response = table.put_item(Item=final_out)
