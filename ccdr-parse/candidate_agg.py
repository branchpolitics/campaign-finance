import datetime
from decimal import Decimal
import heapq
import json

import boto3
from boto3.dynamodb.conditions import Key
from boto3.dynamodb.conditions import Attr

schema_version = '2.1'

def init_dynamo(local=True):
    if local:
        dynamodb = boto3.resource(
            'dynamodb', endpoint_url="http://localhost:8000", region_name="us-east-1")
    else:
        dynamodb = boto3.resource('dynamodb', region_name="us-east-1")
    contribution_table_name = 'campaign_finance_pdf_outputs'
    contributions_table = dynamodb.Table(contribution_table_name)
    candidate_info_table_name = 'campaign_finance_candidate_summary'
    candidate_info_table = dynamodb.Table(candidate_info_table_name)
    return contributions_table, candidate_info_table


def scan_candidates_from_contributions(table, city='atlanta'):
    # TODO: filter by city
    scan_kwargs = {
        'ProjectionExpression': 'filerid'
    }
    done = False
    start_key = None
    candidates = []
    while not done:
        if start_key:
            scan_kwargs['ExclusiveStartKey'] = start_key
        response = table.scan(**scan_kwargs)
        for candidate in response['Items']:
            candidates.append(candidate['filerid'])
        start_key = response.get('LastEvaluatedKey', None)
        done = start_key is None
    candidates = list(set(candidates))
    return candidates


def query_files_per_candidate(table, filerid):
    # data = table.scan(
    #     FilterExpression=Attr('filerid').eq(filerid))
    # return data['Items']
    response = table.query(
        KeyConditionExpression=Key('filerid').eq(filerid)
    )
    return response['Items']


def filter_files_by_reporting_year(doc_list, year_diff=1):
    current_year = datetime.datetime.now().year
    cutoff_year = int(current_year) - year_diff
    filtered_list = []
    problem_list = []
    for doc in doc_list:
        include = False
        filedate = datetime.datetime.strptime(
            doc['input']['datesubmitted'], '%m/%d/%y')
        if int(filedate.year) >= int(cutoff_year):
            try:
                reporting_year = doc['pdf_data']['metadata']['reporting_deadline'][-4:]
                if int(reporting_year) >= int(cutoff_year):
                    include = True
            except:
                problem_list.append(doc)
        if include:
            filtered_list.append(doc)
        pass

    return filtered_list, problem_list


def latest_amendment_check(doc_list, problem_list):
    keep_list = []
    reporting_periods = {}
    # get each doc's period
    for doc in doc_list:
        try:
            doc_period = doc['pdf_data']['metadata']['reporting_deadline']
            doc_id = doc['input']['documentid']
            filedate = datetime.datetime.strptime(
                doc['input']['datesubmitted'], '%m/%d/%y')
            if doc_period not in reporting_periods:
                reporting_periods[doc_period] = []
            reporting_periods[doc_period].append({
                'documentid': doc_id,
                'filedate': filedate,
                'doc': doc
            })
        except Exception as e:
            problem_list.append(doc)
    # find latest file per period
    for period in reporting_periods:
        latest_doc = {}
        for doc in reporting_periods[period]:
            if not latest_doc:
                latest_doc = doc
            else:
                if doc['filedate'] > latest_doc['filedate']:
                    latest_doc = doc
        keep_list.append(latest_doc['doc'])
    return keep_list, problem_list


def get_candidate_info(doc_list):
    doc = doc_list[-1]
    response = {}
    response['filerid'] = doc['input']['filerid']
    response['city'] = doc['city-upload-date'].split('-')[0]
    response['firstname'] = doc['input']['firstname']
    response['lastname'] = doc['input']['lastname']
    response['displayname'] = doc['input']['displayname']
    response['first_last_name'] = response['firstname'].lower() + \
        '_' + response['lastname'].lower()
    response['officename'] = doc['input']['officename']
    response['aggregated_data'] = []
    return response


def strip_money(money):
    money = money.replace('$', '').replace(',', '')
    if money.startswith('(') and money.endswith(')'):
        money = '-' + money.replace('(', '').replace(')', '')
    return float(money)


def get_donation_amount(contribution):
    if 'cash' in contribution:
        cash = strip_money(contribution['cash'])
    else: 
        cash = 0
    if 'value' in contribution:
        value = strip_money(contribution['value'])
    else:
        value = 0
    donation_amount = cash + value
    return donation_amount

def create_donor_key(contribution):
    if 'first_name' in contribution:
        first_name = contribution['first_name']
    else:
        first_name = ''
    if 'last_name' in contribution:
        last_name = contribution['last_name']
    else:
        last_name = ''
    if 'address' in contribution:
        address = contribution['address']
    else:
        address = ''
    donor_key = (first_name, last_name, address)
    return donor_key


def create_employer_key(contribution):
    if 'employer' in contribution:
        employer = contribution['employer']
    else:
        employer = 'UNLISTED'
    employer_key = employer
    return employer_key


def clean_employer_list(employer_list):
    employers_delete_list = ['Retired', 'UNLISTED', 'Self Employed', 'Business owner',
                             'Self', 'Not Employed', 'N/A', 'owner', 'employer_list']
    employers_delete_list_lower = list(map(str.lower, employers_delete_list))
    cleaned_employer_list = {}
    for employer in employer_list:
        if employer.lower() not in employers_delete_list_lower:
            cleaned_employer_list[employer] = employer_list[employer]
    return cleaned_employer_list


def top_n_donors(donor_list, n=10):
    # expecting format individual_donor_list = {key: {'total': 100}}
    # heapq implements only a min heap, so use negative value
    heap = [(-value['total'], key) for key, value in donor_list.items()]
    largest = heapq.nsmallest(10, heap)
    largest = [(key, -value) for value, key in largest]
    return largest


def get_latest_total_contributions(doc_list):
    # TODO: not using this due to candidate mistakes in pdfs
    latest_doc = {}
    max_total = 0
    for doc in doc_list:
        current_filedate = datetime.datetime.strptime(doc['input']['datesubmitted'], '%m/%d/%y')
        if not latest_doc:
            latest_doc = doc
            latest_filedate = datetime.datetime.strptime(doc['input']['datesubmitted'], '%m/%d/%y')
        else:
            if current_filedate > latest_filedate:
                latest_doc = doc
        doc_total = strip_money(doc['pdf_data']['summary_report']['contributions_total_inkind']) + strip_money(doc['pdf_data']['summary_report']['contributions_total_cash'])
        if doc_total > max_total:
            max_total = doc_total
    latest_doc_total = strip_money(latest_doc['pdf_data']['summary_report']['contributions_total_inkind']) + strip_money(latest_doc['pdf_data']['summary_report']['contributions_total_cash'])
    # if latest_doc_total != max_total:
    #     # TODO:
    #     raise(ValueError)
    return latest_doc_total


def aggregate_contributions(doc_list):
    individual_donor_list = {}
    individual_total = 0.0
    corporation_list = {}
    corporation_total = 0.0
    corporation_donation_count = 0
    employer_list = {}
    small_donations = 0.0
    large_donations = 0.0
    small_donations_count = 0
    large_donations_count = 0
    small_donations_individual = 0.0
    large_donations_individual = 0.0
    small_donations_individual_count = 0
    large_donations_individual_count = 0
    donation_count = 0
    donor_count = 0
    total_itemized_amount = 0.0
    total_nonitemized_amount = 0.0
    # total_amount = get_latest_total_contributions(doc_list)
    for doc in doc_list:
        # TODO: get latest reporting period file and get 
        # doc['pdf_data']['summary_report']['contributions_total_inkind'] + 
        # doc['pdf_data']['summary_report']['contributions_total_cash']
        # 
        # add doc_nonitemized_amount to total_nonitemized_amount
        doc_nonitemized_amount = strip_money(doc['pdf_data']['summary_report']['contributions_notlisted_inkind']) + \
            strip_money(doc['pdf_data']['summary_report']['contributions_notlisted_cash'])
        total_nonitemized_amount += doc_nonitemized_amount
        # aggregate each doc's contributions
        for contribution in doc['pdf_data']['itemized_contributions']:
            # get values
            donation_amount = get_donation_amount(contribution)
            donation_count += 1
            total_itemized_amount += donation_amount
            if 'last_name' in contribution:
                # donor is an individual
                # attribute to donor
                donor_key = create_donor_key(contribution)
                if donor_key not in individual_donor_list:
                    individual_donor_list[donor_key] = {'total': 0.0, 'count': 0}
                individual_donor_list[donor_key]['total'] += donation_amount
                individual_donor_list[donor_key]['count'] += 1
                individual_total += donation_amount
                # individual small/large split
                if donation_amount <= 200:  # following OpenSecrets split
                    small_donations_individual += donation_amount
                    small_donations_individual_count += 1
                else:
                    large_donations_individual += donation_amount
                    large_donations_individual_count += 1
            else:
                # donor is a corporation
                # attribute to corporation
                if 'first_name' in contribution:
                    corp_name = contribution['first_name']
                else:
                    corp_name = 'UNLISTED'
                if corp_name not in corporation_list:
                    corporation_list[corp_name] = {'total': 0.0, 'count': 0}
                corporation_list[corp_name]['total'] += donation_amount
                corporation_list[corp_name]['count'] += 1
                corporation_total += donation_amount
                corporation_donation_count += 1
            donor_count = len(individual_donor_list.keys()) + len(corporation_list.keys())
            # attribute to employer
            employer_key = create_employer_key(contribution)
            if employer_key not in employer_list:
                employer_list[employer_key] = {'total': 0.0, 'count': 0}
            employer_list[employer_key]['total'] += donation_amount
            employer_list[employer_key]['count'] += 1
            # split small/large
            if donation_amount <= 200:  # following OpenSecrets split
                small_donations += donation_amount
                small_donations_count += 1
            else:
                large_donations += donation_amount
                large_donations_count += 1
            # END CONTRIBUTION FOR-LOOP
        # END DOC FOR-LOOP
    # look up top donors
    top_individual_donors = top_n_donors(individual_donor_list, n=10)
    # look up top corporations
    top_corporations = top_n_donors(corporation_list, n=10)
    # look up top employers
    employer_list = clean_employer_list(employer_list)
    top_employers = top_n_donors(employer_list, n=10)
    total_amount = total_itemized_amount + total_nonitemized_amount
    # calculate percentages
    small_donations_individual = small_donations_individual + total_nonitemized_amount
    if (total_itemized_amount > 0) and (donation_count > 0):
        pct_small_individual_amount = small_donations_individual / total_amount * 100
        pct_large_individual_amount = large_donations_individual / total_amount * 100
        pct_corporation_amount = corporation_total / total_amount * 100
        pct_small_individual_count = small_donations_individual_count / donation_count * 100
        pct_large_individual_count = large_donations_individual_count / donation_count * 100
        pct_corporation_count = corporation_donation_count / donation_count * 100
    else:
        pct_small_individual_amount = 0
        pct_large_individual_amount = 0
        pct_corporation_amount = 0
        pct_small_individual_count = 0
        pct_large_individual_count = 0
        pct_corporation_count = 0
    # put it together!
    aggregations = {
        'individual_total': individual_total,
        'corporation_total': corporation_total,
        'corporation_donation_count': corporation_donation_count,
        'small_donations': small_donations,
        'large_donations': large_donations,
        'small_donations_count': small_donations_count,
        'large_donations_count': large_donations_count,
        'small_donations_individual': small_donations_individual,
        'large_donations_individual': large_donations_individual,
        'small_donations_individual_count': small_donations_individual_count,
        'large_donations_individual_count': large_donations_individual_count,
        'donation_count': donation_count,
        'donor_count': donor_count,
        'total_amount': total_amount,
        'total_itemized_amount': total_itemized_amount,
        'total_nonitemized_amount': total_nonitemized_amount,
        'pct_small_individual_amount': pct_small_individual_amount,
        'pct_large_individual_amount': pct_large_individual_amount,
        'pct_corporation_amount': pct_corporation_amount,
        'pct_small_individual_count': pct_small_individual_count,
        'pct_large_individual_count': pct_large_individual_count,
        'pct_corporation_count': pct_corporation_count,
        'top_individual_donors': top_individual_donors,
        'top_corporations': top_corporations,
        'top_employers': top_employers,
        'status': 'valid'
    }
    return aggregations


def strip_doc_list(doc_list):
    stripped_docs = []
    for doc in doc_list:
        details = {}
        details['file_name'] = doc['input']['documentname']
        details['datesubmitted'] = doc['input']['datesubmitted']
        details['documentUrl'] = doc['input']['documentUrl']
        stripped_docs.append(details)
    return stripped_docs


def create_failed_aggregation():
    aggregations = {
        'individual_total': 0,
        'corporation_total': 0,
        'corporation_donation_count': 0,
        'small_donations': 0,
        'large_donations': 0,
        'small_donations_count': 0,
        'large_donations_count': 0,
        'small_donations_individual': 0,
        'large_donations_individual': 0,
        'small_donations_individual_count': 0,
        'large_donations_individual_count': 0,
        'donation_count': 0,
        'donor_count': 0,
        'total_amount': 0,
        'total_itemized_amount': 0,
        'total_nonitemized_amount': 0,
        'pct_small_individual_amount': 0,
        'pct_large_individual_amount': 0,
        'pct_corporation_amount': 0,
        'pct_small_individual_count': 0,
        'pct_large_individual_count': 0,
        'pct_corporation_count': 0,
        'top_individual_donors': [],
        'top_corporations': [],
        'top_employers': [],
        'status': 'failure'
    }
    return aggregations


def write_output(table, record):
    response = table.put_item(Item=record)
    return response


def process_candidate(filer_id, contributions_table, candidate_info_table):
    # get all files for filerid
    docs = query_files_per_candidate(contributions_table, filer_id)
    # init response
    aggregated_response = get_candidate_info(docs)
    # filter relevant files
    doc_list, problem_list = filter_files_by_reporting_year(docs)
    doc_list, problem_list = latest_amendment_check(doc_list, problem_list)
    if len(doc_list) > 0:
        aggregated_response['aggregated_data'] = aggregate_contributions(
            doc_list)
    else:
        aggregated_response['aggregated_data'] = create_failed_aggregation()
    # remove extra info from doc lists
    stripped_problem_list = strip_doc_list(problem_list)
    aggregated_response['problem_docs'] = stripped_problem_list
    stripped_doc_list = strip_doc_list(doc_list)
    aggregated_response['aggregated_docs'] = stripped_doc_list
    # set additional metadata
    aggregated_response['key'] = aggregated_response['filerid']
    aggregated_response['process_date'] = str(datetime.datetime.now())
    # convert floats to Decimal for dynamodb
    aggregated_response = json.loads(json.dumps(
        aggregated_response), parse_float=Decimal)
    # TODO: check 400kb size limit
    # save schema version
    aggregated_response['schema_version'] = schema_version
    response = write_output(candidate_info_table, aggregated_response)


def aggregate_all():
    contributions_table, candidate_info_table = init_dynamo()
    candidate_list = scan_candidates_from_contributions(contributions_table)
    for candidate in candidate_list:
        process_candidate(candidate, contributions_table, candidate_info_table)



if __name__ == '__main__':
    pass
    
