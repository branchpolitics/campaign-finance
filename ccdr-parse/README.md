## File overview
1. `parseccdr.py` - Use api_input() to fetch parse a document as output by the filer-api. Outputs to dynamodb table. Run script directly to use sample doc in `__main__`.
    1. Use pdfminer.six library to parse all pages of the PDF, getting each text field and its coordinates per page.
    2. Check each page type by checking page number in top right. This is per page type, not absolute page number.
    3. For each page, use coordinates to map each field's to the appropriate variable. Unmapped text fields are put in an `_unmapped` list for later processing or error handling.
    4. For checkbox-type fields, use the checkbox field mapping to collapse the multiple potential options into a single value.
    5. Process contributions to a single list of entries instead of just page/field mappings
    6. Save output!
2. `candidate_agg.py` - Given a filerid, aggregates all docs in the parsed contributions_table and outputs totals to candidate_info_table.
    1. Get all files per filerid. This is different for each office run even for the same person.
    2. Filter files by reporting year
    3. Filter out to only the latest amendment per reporting date. This needs better logic.
    4. Combine all contributions and tally totals by small/large (<$200) and individual/corporate (corporate has no last name). 
    5. Save output!
3. `pdf_coords.json` - PDF field coordinates and mappings for EasyVote Campaign Contribution Disclosure Report (CCDR) files for Atlanta and most EasyVote systems.
4. Notebooks
    * `parseccdr.ipynb` - Breaks apart parseccdr.py. Useful for debugging parsing errors
    * `process_all.ipynb` - Pipeline to load PDFs document list from db, then fetch and parse those PDFs
    * `candidate_agg.ipynb` - Breaks apart candidate_agg.py for individual debugging and processes all candidates in contributions table.

## High-Level Data Flow
1. filer-api fetches EasyVote document lists to filter for new entries and add them to the 1st file table.
2. parseccdr takes new relevant (city+date) entries to the 1st file table, fetches and parses those PDFs, and outputs structured data per PDF to the 2nd contributions table.
3. candidate_agg aggregates the 2nd contributions table by filerid and adds up totals per filerid to write to 3rd candidate_info table.