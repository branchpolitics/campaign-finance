var doc = require('dynamodb-doc');

const getItem = async ({
  tableName,
  key
}) => {
  var params = {
    Key: key,
    TableName: tableName
  };
  return new Promise((resolve, reject) => {
    var pfunc = function(err, data) {
      if(err) reject(err);
      else resolve(data)
    }
    var docClient = new doc.DynamoDB();
    docClient.getItem(params, pfunc);
  })
}

const queryItem = async ({
  tableName,
  indexName,
  queryString,
  queryStringValues,
  attributes,
  limit = 5
}) => {
  const params = {
    TableName: tableName,
    IndexName: indexName,
    Limit: limit,
    KeyConditionExpression: queryString,
    ExpressionAttributeValues: queryStringValues,
    ...(attributes && attributes.length > 0 ? { ProjectionExpression: attributes.join(',') } : {})
  }
  return new Promise((resolve, reject) => {
    var pfunc = function(err, data) {
      if(err) reject(err);
      else resolve(data)
    }
    var docClient = new doc.DynamoDB();
    docClient.query(params, pfunc);
  })
}

const respond = (statusCode, json) => ({
  statusCode,
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify(json),
})

module.exports = {
  getItem,
  queryItem,
  respond
}
