rm -Rf candidateSearchApiBuild
mkdir candidateSearchApiBuild
cp package.json package.json candidateSearchApiBuild
cp util.js util.js candidateSearchApiBuild
cp util.js util.js candidateSearchApiBuild
cp candidateSearchApi.js candidateSearchApi.js candidateSearchApiBuild
cd candidateSearchApiBuild
npm install

zip -r candidateSearchApi.zip .
aws lambda update-function-code --function-name CFSearchCandidateName --zip-file fileb:///Users/walterley/branch-dev/campaign-finance/filer-api/candidateSearchApiBuild/candidateSearchApi.zip
aws lambda update-function-configuration --function-name CFSearchCandidateName --handler candidateSearchApi.handler --timeout 15 --memory-size 128
rm -Rf candidateSearchApiBuild
