rm -Rf filerSearchBuild
mkdir filerSearchBuild
cp package.json package.json filerSearchBuild
cp util.js util.js filerSearchBuild
cp util.js util.js filerSearchBuild
cp filerSearchApi.js filerSearchApi.js filerSearchBuild
cd filerSearchBuild
npm install

zip -r filerSearchApi.zip .
aws lambda update-function-code --function-name FinanceDataForFilerId --zip-file fileb:///Users/walterley/branch-dev/campaign-finance/filer-api/filerSearchBuild/filerSearchApi.zip
aws lambda update-function-configuration --function-name FinanceDataForFilerId --handler filerSearchApi.handler --timeout 15 --memory-size 128
rm -Rf filerSearchBuild
