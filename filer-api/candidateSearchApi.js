const { queryItem, respond } = require('./util')

const supportedCities = [
  'Atlanta'
]

const mainFunc = async (event) => {
    const {
      city,
      candidateName
    } = (event?.queryStringParameters || {});
    if(event?.requestContext?.http?.method !== "GET") {
      return respond(405, {
        message: 'This API only supports GET requests.'
      })
    }

    if(!city) {
      return respond(400, {
        message: 'Must provide a `city` to search for'
      })
    }

    if(!supportedCities.map(c => c.toLowerCase()).includes(city.toLowerCase())) {
      return respond(400, {
        message: `We do not support the city "${city}"`
      })
    }

    if(!candidateName) {
      return respond(400, {
        message: 'Must provide a `candidateName` to search for'
      })
    }

    let responseJson;
    try {
      const documentRes = await queryItem({
        tableName: 'campaign_finance_candidate_summary',
        indexName: 'city-first_last_name-index',
        queryString: "city = :cityname AND begins_with(first_last_name, :cdname)",
        queryStringValues: {
            ":cityname": city.toLowerCase(),
            ':cdname': `${candidateName}`.toLowerCase().split(' ').join('_')
        },
        attributes: ['city', 'displayname', 'firstname', 'lastname', 'officename', 'filerid', 'process_date']
      })
      responseJson = {
        total: (documentRes.Items || []).length,
        data: (documentRes.Items || [])
      }
    } catch (error) {
      console.error(error);
      return respond(500, error)
    }

    return respond(200, responseJson);
};

exports.handler = mainFunc;
