const mainFunc = require('./candidateSearchApi').handler;


const sampleData = {
    "version": "2.0",
    "routeKey": "ANY /campaignFinanceTest",
    "rawPath": "/default/campaignFinanceTest",
    "rawQueryString": "filerId=1234",
    "headers": {
        "accept-encoding": "gzip, deflate, br",
        "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJpYXQiOjE2MzQ5MTg4NDEsImV4cCI6MTY2NjQ1NDg0MSwiYXVkIjoiaHR0cHM6Ly9icmFuY2guY2hhdCIsImlzcyI6ImZlYXRoZXJzIiwic3ViIjoiNWYzOTg3NTA2NWFkMTkzNWU5NjJhMjExIiwianRpIjoiNDBlYzg4ODktMDUwNi00NThiLTgwYzktYzdiZmZmNDY3MWIwIn0.zbKUXBd_Q31RPL7ZgZULH7uNf5E-jdeQ6PuTv_5n8N0",
        "cache-control": "no-cache",
        "content-length": "4396",
        "content-type": "application/json",
        "host": "haeudl5ugg.execute-api.us-east-1.amazonaws.com",
        "postman-token": "97602247-b985-4587-b8bb-49619c346c18",
        "user-agent": "PostmanRuntime/7.26.8",
        "x-amzn-trace-id": "Root=1-61785965-2133c49e6e8dcacd0ef681ae",
        "x-forwarded-for": "72.187.6.208",
        "x-forwarded-port": "443",
        "x-forwarded-proto": "https"
    },
    "queryStringParameters": {
        "candidateName": "Feli",
        "city": "Atlanta",
    },
    "requestContext": {
        "accountId": "174996727867",
        "apiId": "haeudl5ugg",
        "domainName": "haeudl5ugg.execute-api.us-east-1.amazonaws.com",
        "domainPrefix": "haeudl5ugg",
        "http": {
            "method": "GET",
            "path": "/default/campaignFinanceTest",
            "protocol": "HTTP/1.1",
            "sourceIp": "72.187.6.208",
            "userAgent": "PostmanRuntime/7.26.8"
        },
        "requestId": "H1Ln5htQIAMEJ6g=",
        "routeKey": "ANY /campaignFinanceTest",
        "stage": "default",
        "time": "26/Oct/2021:19:39:17 +0000",
        "timeEpoch": 1635277157584
    },
    "body": "{\n    \"coverageStatus\": \"coverage\",\n    \"commentaryAvailable\": false,\n    \"sources\": [],\n    \"priority\": 0,\n    \"title\": \"DeKalb County ESPLOST\",\n    \"election\": \"ga-2021-general\",\n    \"district\": \"5f398f1bb64437378848473e\",\n    \"descriptionShort\": \"Decides whether or not to renew a 1% county sales tax to fund public schools\",\n    \"ballotText\": \"Shall the special one percent sales and use tax for educational purposes currently imposed in DeKalb County be reimposed on July 1, 2022, upon the expiration of the currently imposed tax, for not longer than 20 consecutive calendar quarters, to raise not more than $816,235,158.00 to be used for the following educational purposes:\\n\\n**For the DeKalb County School District:**\\nAdding to, upgrading, reconfiguring, renovating, replacing, modifying and equipping existing and replacement schools, support facilities, athletic fields and physical education facilities; (B) acquiring and developing land for, constructing and equipping, new and replacement schools and support facilities, as well as additions to existing facilities; (C) installing capital improvements for various existing and new schools, buildings and facilities; (D) providing hardware, software, and related infrastructure and making technology improvements; and (E) replacing, purchasing, upgrading school buses, support vehicles, and other capital equipment, in an amount not to excess $742,773,993.78 for the DeKalb County School District, all as more fully described in the Notice of Election.\\n\\n**For the Atlanta Independent School System:**\\nfor acquiring, reconfiguring and developing land for, constructing and equipping, new and replacement schools and support facilities; (B) adding to, upgrading, reconfiguring, renovating, replacing, modifying and equipping existing and replacement schools, support facilities and athletic facilities; (C) making technology improvements and safety and security improvements for schools and facilities; and (D) acquiring and equipping school buses, school police vehicles, support vehicles, transportation vehicles and other capital equipment. At a total maximum cost of $32,649,406.32, for the Atlanta Independent School System as more fully described in the Notice of Election.\\n\\n**For the City Schools of Decatur:**\\nfor the acquisition, construction, renovation, modification, addition, repair, replacement, demolition of, improvement and equipping of existing and new school buildings and other facilities, including other buildings and greenspace useful or desirable in connection therewith; (B) to acquire and install technology improvements throughout the school system, including computers, systems, and related hardware and infrastructure; (C) to acquire school buses, school-related vehicles, and transportation equipment, including infrastructure for electric vehicles; (D) for school and facilities improvement, energy conservation, and maintenance projects; and (E) to acquire supplemental capital equipment for the school system, including any property, real and personal, useful and desirable in connection with the projects listed in the Notice of Election, in an amount not to exceed $40,811,757,90 for the City Schools of Decatur, all as more fully described in the Notice of Election.\",\n    \"whatItMeans\": \"This referendum is about whether or not to renew the 1% sales tax for school funding. The tax has been in place since 1997. The current tax was approved in 2016 and is set to expire on June 30, 2022 if it is not renewed in this election. The funding from the tax can only be used for projects like building and furnishing schools or buying equipment like computers or buses, not for the day to day operations of the school district. If passed, the tax will raise a maximum of $816 million over the next five years, with about $743 million going to DeKalb County schools, $33 million going to Atlanta Independent schools and $41 million going to Decatur City schools. All of the school systems have school repairs and construction on their project lists, while Decatur City schools also included energy conservation projects and Atlanta Independent schools included new school police vehicles.\",\n    \"supportersSay\": \"County sales taxes stay at their current rate, and the school projects will be funded.\",\n    \"opponentsSay\": \"County sales taxes fall by 1%, and the school projects will not be funded.\"\n}",
    "isBase64Encoded": false
}

const run = async () => {
  const res = await mainFunc(sampleData)
  console.log(res)
}

run()
