const { getItem, respond } = require('./util')

const mainFunc = async (event) => {
    const {
      filerId
    } = (event?.queryStringParameters || {});
    console.log('received event...')
    console.log(JSON.stringify(event))
    if(event?.requestContext?.http?.method !== "GET") {
      return respond(405, {
        message: 'This API only supports GET requests.'
      })
    }

    if(!filerId) {
      return respond(400, {
        message: 'Must provide a valid `filerId` to search for'
      })
    }

    let responseJson;
    try {
      const financeForFiler = await getItem({
        key: { 'key' : filerId },
        tableName: 'campaign_finance_candidate_summary'
      })
      if(financeForFiler?.Item) {
        responseJson = financeForFiler.Item;
      } else {
        return respond(404, {
          message: `No record found for filerId ${filerId}`
        })
      }
    } catch (error) {
      console.error(error);
      return respond(500, error)
    }

    return respond(200, responseJson)
};

exports.handler = mainFunc;
